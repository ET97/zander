import Vue from 'vue';
import Router from 'vue-router';
import Query from './components/Query.vue';
import Main from './components/Main.vue';
import Config from './components/Config.vue';
import Help from './components/Help.vue';
import AccumuloGui from './components/AccumuloGui.vue';
import Todo from './components/Todo.vue';
import ToDoList from './components/TodoList.vue';
import TodoAdd from './components/TodoAdd.vue';
import Links from './components/Links.vue';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/accumulo',
      name: 'accumulo',
      component: AccumuloGui,
      children: [
        {
          path: '/accumulo/query',
          name: 'query',
          component: Query
        },
        {
          path: '/accumulo/config',
          name: 'config',
          component: Config
        },
        {
          path: '/accumulo/help',
          name: 'help',
          component: Help
        }
      ]
    },
    {
      path: '/todo',
      name: 'todo',
      component: Todo,
      children: [
        {
          path: '/todo/list',
          name: 'list',
          component: ToDoList
        },
        {
          path: '/todo/add',
          name: 'add',
          component: TodoAdd
        }
      ]
    },
    {
      path: '/links',
      name: 'links',
      component: Links,
    }
  ]
})
