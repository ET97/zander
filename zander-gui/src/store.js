import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    query_ids: [2,45],
    query_ids_empty: false,
    query_current_doc: "document",
    query_current_id: 8,
    config_host: null,
    config_port: null,
    config_user: null,
    config_password: null,
    todo_list: [],
  },
  getters: {
    query_countIds: state => {
      return state.query_ids.length;
    },
    todo_countTodos: state => {
      return state.todo_list.length;
    },
    todo_sortArray: state => {
      function compare(todo1, todo2) {
        if (todo1.date < todo2.date)
          return -1;
        if (todo1.date > todo2.date)
          return 1;
        return 0;
      }
      return state.todo_list.sort(compare);
    }
  },
  mutations: {
    QUERY_SET_IDS: (state, newIds) => {
      state.query_ids = newIds;
    },
    QUERY_SET_IDS_EMPTY: (state, id_empty) => {
      state.query_ids_empty = id_empty;
    },
    QUERY_SET_CURRENT_ID: (state,newId) => {
      state.query_current_id = newId;
    },
    QUERY_SET_CURRENT_DOC: (state,newDoc) => {
      state.query_current_doc = newDoc;
    },
    CONFIG_SET_HOST: (state, newHost) => {
      state.config_host = newHost;
    },
    CONFIG_SET_PORT: (state, newPort) => {
      state.config_port = newPort;
    },
    CONFIG_SET_USER: (state, newUser) => {
      state.config_user = newUser;
    },
    CONFIG_SET_PASSWORD: (state, newPassword) => {
      state.config_password = newPassword;
    },
    TODO_ADD: (state, newTodo) => {
      state.todo_list.push(newTodo);
    },
    TODO_REMOVE: (state, todo) => {
      state.todo_list.splice(todo, 1);
    }
  },
  actions: {}
})
