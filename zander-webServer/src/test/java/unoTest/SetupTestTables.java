package unoTest;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Instance;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

import accumulo.dao.GenericDao;
import domain.ValueObject;
import webServer.WebServerConfig;

public class SetupTestTables {

	public static void main(String[] args) throws Exception {
		WebServerConfig config = new WebServerConfig(args[0]);
		Instance instance = new ZooKeeperInstance(config.getInstanceName(), config.getZookeepers());
		Connector connector = instance.getConnector(config.getPrincipal(), new PasswordToken(config.getPassword()));
		TableOperations to = connector.tableOperations();
		to.create(config.getTable());
		to.create(config.getTable() + "_index");

		GenericDao dao = createDao(config);
		for (int i = 0; i < 20; i++) {
			dao.insert(new Communication(i));
		}

	}

	private static GenericDao createDao(WebServerConfig config)
			throws AccumuloException, AccumuloSecurityException, TableNotFoundException {
		Instance instance = new ZooKeeperInstance(config.getInstanceName(), config.getZookeepers());
		Connector connector = instance.getConnector(config.getPrincipal(), new PasswordToken(config.getPassword()));
		return new GenericDao(connector, config.getAuthorizations(), config.getTable(), config.getTable() + "_index",
				config.getMaxNrIndices());
	}

	private static class Communication extends ValueObject {
		private Communication(int i) {
			super();
			addIndex("sender", "sender" + i);
			addIndex("receiver", "receiver" + i);
			addIndex("type", "email");
			setContent(("content" + i).getBytes());
		}

		@Override
		public String toString() {
			return getIndices().toString() + " : " + new String(getContent());
		}
	}

}
