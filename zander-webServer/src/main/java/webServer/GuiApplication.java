package webServer;

import javax.inject.Inject;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import accumulo.dao.GenericDao;
import resources.QueryResource;

public class GuiApplication extends ResourceConfig {

	@Inject
	public GuiApplication(GenericDao dao) {

		System.out.println("setting up hk2");
		packages("webServer", "resources");

		register(QueryResource.class);

		JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
		jacksonJaxbJsonProvider.setMapper(new ObjectMapper());
		register(jacksonJaxbJsonProvider);

		register(CorsFilter.class);

		register(new AbstractBinder() {

			@Override
			protected void configure() {
				bind(dao).to(GenericDao.class);
			}

		});
	}
}