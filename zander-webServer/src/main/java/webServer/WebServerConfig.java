package webServer;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.accumulo.core.security.Authorizations;

public class WebServerConfig {

	private int port;
	private int maxNrIndices;
	private String zookeepers;
	private String instanceName;
	private String principal;
	private String password;
	private String table;
	private Authorizations authorizations;

	public WebServerConfig(String pathToCopnfigFile) throws Exception {
		Properties properties = new Properties();
		properties.load(new FileInputStream(pathToCopnfigFile));
		maxNrIndices = Integer.parseInt((String) properties.getOrDefault("query.maxNrIndices", "100"));
		port = Integer.parseInt(properties.getProperty("webServer.port"));
		if (port < 1024) {
			throw new IllegalArgumentException("Only ports greater than 1024 are allowed!");
		}
		zookeepers = properties.getProperty("accumulo.zookeepers");
		if (zookeepers == null) {
			throw new IllegalArgumentException("'accumulo.zookeepers' property is missing!");
		}
		instanceName = properties.getProperty("accumulo.instanceName");
		if (instanceName == null) {
			throw new IllegalArgumentException("'accumulo.instanceName' property is missing!");
		}
		principal = properties.getProperty("accumulo.principal");
		if (principal == null) {
			throw new IllegalArgumentException("'accumulo.principal' property is missing!");
		}
		password = properties.getProperty("accumulo.password");
		if (password == null) {
			throw new IllegalArgumentException("'accumulo.password' property is missing!");
		}
		table = properties.getProperty("accumulo.table");
		if (table == null) {
			throw new IllegalArgumentException("'accumulo.table' property is missing!");
		}
		String authorizationsString = properties.getProperty("accumulo.authorizations");
		if (authorizationsString == null) {
			authorizations = new Authorizations();
		} else {
			authorizations = new Authorizations(authorizationsString);
		}
	}

	public int getMaxNrIndices() {
		return maxNrIndices;
	}

	public int getPort() {
		return port;
	}

	public String getZookeepers() {
		return zookeepers;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public String getPrincipal() {
		return principal;
	}

	public String getPassword() {
		return password;
	}

	public String getTable() {
		return table;
	}

	public Authorizations getAuthorizations() {
		return authorizations;
	}
}
