package webServer;

import java.io.IOException;
import java.net.URI;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Instance;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import accumulo.dao.GenericDao;

public class GrizzlyRestServer {

	public static void main(String[] args) throws Exception {
		WebServerConfig config = new WebServerConfig(args[0]);
		String BASE_URI = "http://localhost:" + config.getPort();
		ServiceLocator locator = ServiceLocatorUtilities.createAndPopulateServiceLocator();

		GenericDao dao = createDao(config);
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), new GuiApplication(dao),
				locator);

		try {
			httpServer.start();

			System.out.println(String.format("zander app started.\nHit enter to stop it...", BASE_URI));
			System.in.read();
		} catch (IOException e) {
			System.err.println("error starting server: " + e.getLocalizedMessage());
		}
	}

	private static GenericDao createDao(WebServerConfig config)
			throws AccumuloException, AccumuloSecurityException, TableNotFoundException {
		Instance instance = new ZooKeeperInstance(config.getInstanceName(), config.getZookeepers());
		Connector connector = instance.getConnector(config.getPrincipal(), new PasswordToken(config.getPassword()));
		return new GenericDao(connector, config.getAuthorizations(), config.getTable(), config.getTable() + "_index",
				config.getMaxNrIndices());
	}
}
