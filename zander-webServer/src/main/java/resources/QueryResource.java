package resources;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;
import org.roaringbitmap.longlong.LongIterator;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.dao.GenericDao;

@Path("query")
public class QueryResource extends Application {

	private GenericDao dao;
	private int maxNrIndices;

	@Inject
	public QueryResource(GenericDao dao) {
		this.dao = dao;
		this.maxNrIndices = dao.getMaxNrIndices();
	}

	// curl -v -X POST http://localhost:9999/query/queryIndex -H "Content-Type:
	// application/json" -d '{"sender": "sender1"}'
	@POST
	@Path("queryIndex")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String queryIndex(String query) throws Exception {
		System.out.println(query);
		Roaring64NavigableMap bitmap = dao.queryIndex(query);
		LongIterator iter = bitmap.getReverseLongIterator();
		int i = 0;
		StringBuilder sb = new StringBuilder("[");
		boolean found = false;
		while (iter.hasNext() && i < maxNrIndices) {
			found = true;
			sb.append(iter.next() + ",");
		}
		if (found) {
			sb.deleteCharAt(sb.length() - 1);
			String result = sb.append("]").toString();
			System.out.println(result);
			return result;
		}
		return "[]";
	}

	@GET
	@Path("document/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String readSingleDocument(@PathParam("id") long docId) throws Exception {
		String content = new String(dao.readSingleDocument(docId));
		return content;
	}

	@POST
	@Path("documents")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String readDocuments(String list) throws Exception {
		JSONArray ids = new JSONArray(list);
		int nrIds = ids.length();
		Iterator<Object> iter = ids.iterator();
		long[] idArray = new long[nrIds];
		for (int i = 0; i < nrIds; i++) {
			idArray[i] = (long) iter.next();
		}
		Roaring64NavigableMap bitmap = Roaring64NavigableMap.bitmapOf(idArray);
		List<byte[]> docs = dao.readDocuments(bitmap, nrIds);
		StringBuilder sb = new StringBuilder("[\n");
		docs.forEach(ba -> sb.append(byteArrayToJson(ba) + ",\n"));
		sb.deleteCharAt(sb.length() - 2);
		return sb.append("]").toString();
	}

	private String byteArrayToJson(byte[] ba) {
		JSONObject json = new JSONObject(new String(ba));
		return json.toString(4);
	}
}
