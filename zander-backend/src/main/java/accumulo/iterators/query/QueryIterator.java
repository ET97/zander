package accumulo.iterators.query;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;

public interface QueryIterator extends SortedKeyValueIterator<Key, Value> {

	public String describeAsJson();
}
