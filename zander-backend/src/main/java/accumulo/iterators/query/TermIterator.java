package accumulo.iterators.query;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.ArrayByteSequence;
import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.htrace.fasterxml.jackson.core.JsonParseException;
import org.apache.htrace.fasterxml.jackson.databind.JsonMappingException;
import org.apache.htrace.fasterxml.jackson.databind.ObjectMapper;

public class TermIterator implements QueryIterator {

	public static final String FIELD = "field";
	public static final String VALUE = "value";

	private SortedKeyValueIterator<Key, Value> source;
	private String searchField;
	private String searchValue;

	protected TermIterator(SortedKeyValueIterator<Key, Value> source, String searchField, String searchValue) {
		this.source = source;
		this.searchField = searchField;
		this.searchValue = searchValue;
	}

	public TermIterator() {
	};

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		this.source = source;
		if (!options.containsKey(FIELD)) {
			throw new RuntimeException("Option named 'searchField' is mandatory!");
		}
		searchField = options.get(FIELD);
		if (!options.containsKey(VALUE)) {
			throw new RuntimeException("Option named 'searchValue' is mandatory!");
		}
		searchValue = options.get(VALUE);
	}

	@Override
	public boolean hasTop() {
		return source.hasTop();
	}

	@Override
	public void next() throws IOException {
		source.next();
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies, boolean inclusive) throws IOException {
		String searchTerm = searchField + ":" + searchValue;
		source.seek(range, Collections.singletonList(new ArrayByteSequence(searchTerm)), true);
	}

	@Override
	public Key getTopKey() {
		return source.getTopKey();
	}

	@Override
	public Value getTopValue() {
		return source.getTopValue();
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		return new TermIterator(source.deepCopy(env), searchField, searchValue);
	}

	@Override
	public String describeAsJson() {
		return new String(
				"{\n\"type\": \"TERM\",\n\"field\": \"" + searchField + "\",\n\"value\": \"" + searchValue + "\"\n}");
	}

	public static TermIterator createFromJson(SortedKeyValueIterator<Key, Value> source, String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonDescription desc = mapper.readValue(json, JsonDescription.class);
		if (!"TERM".equals(desc.type)) {
			throw new RuntimeException("Json does not describe TermIterator!");
		}
		return new TermIterator(source, desc.field, desc.value);
	}

	public static IteratorSetting getIteratorSetting(int priority, String searchField, String searchValue) {
		IteratorSetting setting = new IteratorSetting(priority, new TermIterator().getClass());
		setting.addOption(FIELD, searchField);
		setting.addOption(VALUE, searchValue);
		return setting;
	}

	public static class JsonDescription {
		private String type;
		private String field;
		private String value;

		public void setType(String type) {
			this.type = type;
		}

		public void setField(String field) {
			this.field = field;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

}
