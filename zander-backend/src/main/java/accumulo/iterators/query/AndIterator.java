package accumulo.iterators.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.PartialKey;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.io.Text;
import org.apache.htrace.fasterxml.jackson.core.JsonParseException;
import org.apache.htrace.fasterxml.jackson.databind.JsonMappingException;
import org.json.JSONObject;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.util.CreationUtil;
import accumulo.iterators.util.ProxyEnv;
import accumulo.iterators.util.RingIterator;

public class AndIterator implements QueryIterator {

	private static final Roaring64NavigableMapEncoder ENCODER = new Roaring64NavigableMapEncoder();
	private static final String DESCRIPTION = "description";

	private RingIterator<QueryIterator> childIterators;
	private int nrChildIterators;
	private boolean inclusive;
	private Key topKey = null;
	private Value topValue = null;
	private boolean hasTop = false;
	private Range range;
	private Roaring64NavigableMap currentBitmap;
	private Text currentShard;

	public AndIterator(List<QueryIterator> childIterators) {
		this.childIterators = new RingIterator<>(childIterators);
		nrChildIterators = childIterators.size();
		if (nrChildIterators < 2) {
			throw new RuntimeException("AndIterator needs at least 2 childs!");
		}
	}

	public AndIterator() {
	};

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		IteratorEnvironment thisEnv = new ProxyEnv(env);
		String json = options.get(DESCRIPTION);
		JSONObject jsonObj = new JSONObject(json);
		if (!"AND".equals(jsonObj.getString("type"))) {
			throw new RuntimeException("Description does not describe AndIterator!");
		}
		List<QueryIterator> children = CreationUtil.createChildrenFromJson(source.deepCopy(thisEnv), thisEnv,
				jsonObj.getJSONArray("children"));
		childIterators = new RingIterator<>(children);
		nrChildIterators = children.size();
	}

	@Override
	public boolean hasTop() {
		return hasTop;
	}

	@Override
	public void next() throws IOException {
		Optional<Pair<Key, Value>> res = advanceToNextMatch(range);
		if (!res.isPresent()) {
			hasTop = false;
			topValue = null;
			topKey = null;
			return;
		}
		hasTop = true;
		topKey = res.get().getKey();
		topValue = res.get().getValue();
		this.range = range.clip(new Range(topKey.followingKey(PartialKey.ROW), null));
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies, boolean inclusive) throws IOException {
		this.inclusive = inclusive;
		Optional<Pair<Key, Value>> res = advanceToNextMatch(range);
		if (!res.isPresent()) {
			hasTop = false;
			topValue = null;
			topKey = null;
			return;
		}
		hasTop = true;
		topKey = res.get().getKey();
		topValue = res.get().getValue();
		this.range = range.clip(new Range(topKey.followingKey(PartialKey.ROW), null));
	}

	@Override
	public Key getTopKey() {
		return topKey;
	}

	@Override
	public Value getTopValue() {
		return topValue;
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		List<QueryIterator> childIters = new ArrayList<>(nrChildIterators);
		childIterators.forEachRemaining(iter -> childIters.add((QueryIterator) iter.deepCopy(env)));
		return new AndIterator(childIters);
	}

	@Override
	public String describeAsJson() {
		StringBuilder sb = new StringBuilder("{\n\"type\": \"AND\",\n\"children\": [\n");
		childIterators.getList().forEach(iter -> sb.append(iter.describeAsJson() + ",\n"));
		return sb.deleteCharAt(sb.length() - 2).append("]\n}").toString();
	}

	private Optional<Pair<Key, Value>> advanceToNextMatch(Range range) {
		int found = 0;
		Range currentRange = new Range(range);
		currentBitmap = null;
		currentShard = null;
		while (true) {
			Optional<Pair<Key, Value>> childResult = advanceToNext(currentRange);
			if (!childResult.isPresent()) {
				return Optional.empty();
			}
			if (currentBitmap == null) { // first run or first run after finding a result or first run after discarding
											// a possible result
				currentBitmap = ENCODER.decode(childResult.get().getValue().get());
				currentShard = childResult.get().getKey().getRow();
				currentRange = currentRange.clip(new Range(childResult.get().getKey().getRow(), null));
			} else {
				if (!currentShard.equals(childResult.get().getKey().getRow())) {
					currentBitmap = ENCODER.decode(childResult.get().getValue().get());
					currentShard = childResult.get().getKey().getRow();
					found = 1;
					currentRange = currentRange.clip(new Range(childResult.get().getKey().getRow(), null));
					continue;
				}
				currentBitmap.and(ENCODER.decode(childResult.get().getValue().get()));
			}
			if (!currentBitmap.isEmpty()) {
				found++;
			} else {
				found = 0;
				currentRange = currentRange
						.clip(new Range(childResult.get().getKey().followingKey(PartialKey.ROW), null));
				currentBitmap = null;
			}
			if (found == nrChildIterators) {
				Key key = new Key(childResult.get().getKey().getRow());
				Value value = new Value(ENCODER.encode(currentBitmap).clone());
				return Optional.of(new Pair<>(key, value));
			}
		}
	}

	private Optional<Pair<Key, Value>> advanceToNext(Range range) {
		if (!childIterators.hasNext()) {
			return Optional.empty();
		}
		SortedKeyValueIterator<Key, Value> child = childIterators.next();
		try {
			child.seek(range, null, inclusive);
			if (!child.hasTop()) {
				return Optional.empty();
			}
			return Optional.of(new Pair<>(child.getTopKey(), child.getTopValue()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static IteratorSetting getIteratorSetting(int priority, String jsonDescription) {
		IteratorSetting setting = new IteratorSetting(priority, new AndIterator().getClass());
		setting.addOption(DESCRIPTION, jsonDescription);
		return setting;
	}

	public static AndIterator createFromJson(SortedKeyValueIterator<Key, Value> source, IteratorEnvironment env,
			String json) throws JsonParseException, JsonMappingException, IOException {
		JSONObject jsonObj = new JSONObject(json);
		String type = jsonObj.getString("type");
		if (!"AND".equals(type)) {
			throw new RuntimeException("JSON does not describe AndIterator!");
		}
		return new AndIterator(
				CreationUtil.createChildrenFromJson(source.deepCopy(env), env, jsonObj.getJSONArray("children")));
	}
}
