package accumulo.iterators.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.io.Text;
import org.apache.htrace.fasterxml.jackson.core.JsonParseException;
import org.apache.htrace.fasterxml.jackson.databind.JsonMappingException;
import org.json.JSONObject;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.util.CreationUtil;
import accumulo.iterators.util.ProxyEnv;

public class OrIterator implements QueryIterator {

	private static final Roaring64NavigableMapEncoder ENCODER = new Roaring64NavigableMapEncoder();
	private static final String DESCRIPTION = "description";

	private Key topKey = null;
	private Value topValue = null;
	private boolean hasTop = false;
	private List<QueryIterator> childIterators;
	private Map<QueryIterator, Pair<Text, byte[]>> childResults;

	public OrIterator(List<QueryIterator> childIterators) {
		this.childIterators = childIterators;
		childResults = new HashMap<>(childIterators.size());
	}

	public OrIterator() {
	}

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		IteratorEnvironment thisEnv = new ProxyEnv(env);
		String json = options.get(DESCRIPTION);
		JSONObject jsonObj = new JSONObject(json);
		if (!"OR".equals(jsonObj.getString("type"))) {
			throw new RuntimeException("Description does not describe OrIterator!");
		}
		List<QueryIterator> children = CreationUtil.createChildrenFromJson(source, thisEnv,
				jsonObj.getJSONArray("children"));
		childIterators = children;
		childResults = new HashMap<>(children.size());
	}

	@Override
	public boolean hasTop() {
		return hasTop;
	}

	@Override
	public void next() throws IOException {
		List<QueryIterator> queryIterators = new ArrayList<>(childResults.keySet().size());
		queryIterators.addAll(childResults.keySet());
		for (QueryIterator child : queryIterators) {
			if (topKey.getRow().equals(childResults.get(child).getKey())) {
				child.next();
				if (child.hasTop()) {
					childResults.put(child, new Pair<>(child.getTopKey().getRow(), child.getTopValue().get()));
				} else {
					childResults.remove(child);
				}
			}
		}
		if (childResults.isEmpty()) {
			hasTop = false;
			topKey = null;
			topValue = null;
		} else {
			hasTop = true;
			Pair<Text, Roaring64NavigableMap> top = linkLowestResults(childResults.values());
			topKey = new Key(top.getKey().getBytes());
			topValue = new Value(ENCODER.encode(top.getValue()));
		}
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies, boolean inclusive) throws IOException {
		for (QueryIterator child : childIterators) {
			child.seek(range, columnFamilies, inclusive);
			if (child.hasTop()) {
				childResults.put(child, new Pair<>(child.getTopKey().getRow(), child.getTopValue().get()));
			}
		}
		if (childResults.isEmpty()) {
			hasTop = false;
			topKey = null;
			topValue = null;
		} else {
			hasTop = true;
			Pair<Text, Roaring64NavigableMap> top = linkLowestResults(childResults.values());
			topKey = new Key(top.getKey().getBytes());
			topValue = new Value(ENCODER.encode(top.getValue()));
		}
	}

	@Override
	public Key getTopKey() {
		return topKey;
	}

	@Override
	public Value getTopValue() {
		return topValue;
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		List<QueryIterator> childIters = new ArrayList<>(childIterators.size());
		childIterators.forEach(iter -> childIters.add((QueryIterator) iter.deepCopy(env)));
		return new OrIterator(childIters);
	}

	@Override
	public String describeAsJson() {
		StringBuilder sb = new StringBuilder("{\n\"type\": \"OR\",\n\"children\": [\n");
		childIterators.forEach(iter -> sb.append(iter.describeAsJson() + ",\n"));
		return sb.deleteCharAt(sb.length() - 2).append("]\n}").toString();
	}

	private Pair<Text, Roaring64NavigableMap> linkLowestResults(Collection<Pair<Text, byte[]>> results) {
		Text minShard = null;
		Roaring64NavigableMap bitmap = null;
		for (Pair<Text, byte[]> thisPair : results) {
			if (minShard == null || thisPair.getKey().compareTo(minShard) < 0) {
				minShard = thisPair.getKey();
				bitmap = ENCODER.decode(thisPair.getValue());
			} else if (minShard.equals(thisPair.getKey())) {
				bitmap.or(ENCODER.decode(thisPair.getValue()));
			}
		}
		return new Pair<>(minShard, bitmap);
	}

	public static OrIterator createFromJson(SortedKeyValueIterator<Key, Value> source, IteratorEnvironment env,
			String json) throws JsonParseException, JsonMappingException, IOException {
		JSONObject jsonObj = new JSONObject(json);
		String type = jsonObj.getString("type");
		if (!"OR".equals(type)) {
			throw new RuntimeException("JSON does not describe OrIterator!");
		}
		return new OrIterator(CreationUtil.createChildrenFromJson(source, env, jsonObj.getJSONArray("children")));
	}

	public static IteratorSetting getIteratorSetting(int priority, String jsonDescription) {
		IteratorSetting setting = new IteratorSetting(priority, new OrIterator().getClass());
		setting.addOption(DESCRIPTION, jsonDescription);
		return setting;
	}

}
