package accumulo.iterators.query;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.hadoop.io.Text;
import org.apache.htrace.fasterxml.jackson.core.JsonParseException;
import org.apache.htrace.fasterxml.jackson.databind.JsonMappingException;
import org.json.JSONObject;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.util.CreationUtil;
import accumulo.iterators.util.ProxyEnv;

public class AndNotIterator implements QueryIterator {
	private static final Roaring64NavigableMapEncoder ENCODER = new Roaring64NavigableMapEncoder();
	private static final String DESCRIPTION = "description";

	private boolean inclusive;
	private Key topKey = null;
	private Value topValue = null;
	private boolean hasTop = false;

	private QueryIterator posChild;
	private QueryIterator negChild;

	public AndNotIterator(QueryIterator posChild, QueryIterator negChild) {
		this.posChild = posChild;
		this.negChild = negChild;
	}

	public AndNotIterator() {
	}

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		IteratorEnvironment thisEnv = new ProxyEnv(env);
		String json = options.get(DESCRIPTION);
		JSONObject jsonObj = new JSONObject(json);
		if (!"ANDNOT".equals(jsonObj.getString("type"))) {
			throw new RuntimeException("Description does not describe AndNotIterator!");
		}
		posChild = CreationUtil.createFromJson(source, env, jsonObj.getJSONObject("posChild"));
		negChild = CreationUtil.createFromJson(source.deepCopy(env), thisEnv, jsonObj.getJSONObject("negChild"));
	}

	@Override
	public boolean hasTop() {
		return hasTop;
	}

	@Override
	public void next() throws IOException {
		posChild.next();
		if (!posChild.hasTop()) {
			hasTop = false;
			topKey = null;
			topValue = null;
			return;
		}
		hasTop = true;
		Text thisRow = posChild.getTopKey().getRow();
		topKey = new Key(thisRow);
		Range thisRange = new Range(thisRow);
		negChild.seek(thisRange, null, inclusive);
		if (!negChild.hasTop()) {
			topValue = posChild.getTopValue();
			return;
		}
		Roaring64NavigableMap bitmap = ENCODER.decode(posChild.getTopValue().get());
		bitmap.andNot(ENCODER.decode(negChild.getTopValue().get()));
		topValue = new Value(ENCODER.encode(bitmap));
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies, boolean inclusive) throws IOException {
		posChild.seek(range, columnFamilies, inclusive);
		if (!posChild.hasTop()) {
			hasTop = false;
			topKey = null;
			topValue = null;
			return;
		}
		hasTop = true;
		Text thisRow = posChild.getTopKey().getRow();
		topKey = new Key(thisRow);
		Range thisRange = new Range(thisRow);
		negChild.seek(thisRange, columnFamilies, inclusive);
		if (!negChild.hasTop()) {
			topValue = posChild.getTopValue();
			return;
		}
		Roaring64NavigableMap bitmap = ENCODER.decode(posChild.getTopValue().get());
		bitmap.andNot(ENCODER.decode(negChild.getTopValue().get()));
		topValue = new Value(ENCODER.encode(bitmap));

	}

	@Override
	public Key getTopKey() {
		return topKey;
	}

	@Override
	public Value getTopValue() {
		return topValue;
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		QueryIterator child1 = (QueryIterator) posChild.deepCopy(env);
		QueryIterator child2 = (QueryIterator) negChild.deepCopy(env);
		return new AndNotIterator(child1, child2);
	}

	@Override
	public String describeAsJson() {
		return "{\n\"type\": \"ANDNOT\",\n\"posChild\": " + posChild.describeAsJson() + ",\n\"negChild\": "
				+ negChild.describeAsJson() + "\n}";
	}

	public static AndNotIterator createFromJson(SortedKeyValueIterator<Key, Value> source, IteratorEnvironment env,
			String json) throws JsonParseException, JsonMappingException, IOException {
		JSONObject jsonObj = new JSONObject(json);
		String type = jsonObj.getString("type");
		if (!"ANDNOT".equals(type)) {
			throw new RuntimeException("JSON does not describe AndNotIterator!");
		}
		return new AndNotIterator(
				CreationUtil.createFromJson(source.deepCopy(env), env, jsonObj.getJSONObject("posChild")),
				CreationUtil.createFromJson(source.deepCopy(env), env, jsonObj.getJSONObject("negChild")));
	}

	public static IteratorSetting getIteratorSetting(int priority, String jsonDescription) {
		IteratorSetting setting = new IteratorSetting(priority, new AndNotIterator().getClass());
		setting.addOption(DESCRIPTION, jsonDescription);
		return setting;
	}
}
