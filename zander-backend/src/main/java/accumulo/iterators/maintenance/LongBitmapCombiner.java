package accumulo.iterators.maintenance;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.TypedValueCombiner;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;

public class LongBitmapCombiner extends TypedValueCombiner<Roaring64NavigableMap> {

	@Override
	public Roaring64NavigableMap typedReduce(Key key, Iterator<Roaring64NavigableMap> iter) {
		Roaring64NavigableMap result = iter.next();
		while (iter.hasNext()) {
			result.or(iter.next());
		}
		return result;
	}

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		super.init(source, options, env);
		setEncoder(new Roaring64NavigableMapEncoder());
	}
}