package accumulo.iterators.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.htrace.fasterxml.jackson.core.JsonParseException;
import org.apache.htrace.fasterxml.jackson.databind.JsonMappingException;
import org.json.JSONArray;
import org.json.JSONObject;

import accumulo.iterators.query.AndIterator;
import accumulo.iterators.query.AndNotIterator;
import accumulo.iterators.query.OrIterator;
import accumulo.iterators.query.QueryIterator;
import accumulo.iterators.query.TermIterator;
import accumulo.iterators.query.XorIterator;

public class CreationUtil {

	private CreationUtil() {
	}

	public static List<QueryIterator> createChildrenFromJson(SortedKeyValueIterator<Key, Value> source,
			IteratorEnvironment env, JSONArray childs) throws JsonParseException, JsonMappingException, IOException {
		int nrChilds = childs.length();
		if (nrChilds < 2) {
			throw new RuntimeException("Iterator needs at least 2 children!");
		}
		List<QueryIterator> childIterators = new ArrayList<>(nrChilds);
		for (int i = 0; i < nrChilds; i++) {
			JSONObject child = childs.getJSONObject(i);
			switch (child.getString("type")) {
			case "TERM":
				childIterators.add(TermIterator.createFromJson(source.deepCopy(env), child.toString()));
				break;
			case "AND":
				childIterators.add(AndIterator.createFromJson(source.deepCopy(env), env, child.toString()));
				break;
			case "OR":
				childIterators.add(OrIterator.createFromJson(source.deepCopy(env), env, child.toString()));
				break;
			case "XOR":
				childIterators.add(XorIterator.createFromJson(source.deepCopy(env), env, child.toString()));
				break;
			case "ANDNOT":
				childIterators.add(AndNotIterator.createFromJson(source.deepCopy(env), env, child.toString()));
				break;
			default:
				throw new RuntimeException("Iterator type of child is unknown!");
			}
		}
		return childIterators;
	}

	public static QueryIterator createFromJson(SortedKeyValueIterator<Key, Value> source, IteratorEnvironment env,
			JSONObject jsonObj) throws JsonParseException, JsonMappingException, IOException {
		switch (jsonObj.getString("type")) {
		case "TERM":
			return TermIterator.createFromJson(source.deepCopy(env), jsonObj.toString());
		case "AND":
			return AndIterator.createFromJson(source.deepCopy(env), env, jsonObj.toString());
		case "OR":
			return OrIterator.createFromJson(source.deepCopy(env), env, jsonObj.toString());
		case "XOR":
			return XorIterator.createFromJson(source.deepCopy(env), env, jsonObj.toString());
		case "ANDNOT":
			return AndNotIterator.createFromJson(source.deepCopy(env), env, jsonObj.toString());
		default:
			throw new RuntimeException("Iterator type is unknown!");
		}
	}

}
