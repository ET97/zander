package accumulo.util;

import java.util.TreeSet;

import org.apache.hadoop.io.Text;

public class IdUtil {

	private IdUtil() {
	}

	public static String reverseId(String docId) {
		return new StringBuilder(docId).reverse().toString();
	}

	public static TreeSet<Text> createSplits(int nrSplits) {
		TreeSet<Text> result = new TreeSet<>();
		for (int i = 0; i < nrSplits; i++) {
			result.add(new Text(String.format("%02d", i)));
		}
		return result;
	}
}
