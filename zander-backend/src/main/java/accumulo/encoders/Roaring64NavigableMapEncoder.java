package accumulo.encoders;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.accumulo.core.iterators.TypedValueCombiner.Encoder;
import org.apache.accumulo.core.iterators.ValueFormatException;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

public class Roaring64NavigableMapEncoder implements Encoder<Roaring64NavigableMap> {

	public Roaring64NavigableMapEncoder() {
	}

	@Override
	public byte[] encode(Roaring64NavigableMap v) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			DataOutput out = new DataOutputStream(bos);
			v.serialize(out);
			return bos.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Roaring64NavigableMap decode(byte[] b) throws ValueFormatException {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(b);
			DataInput in = new DataInputStream(bis);
			Roaring64NavigableMap newBitmap = new Roaring64NavigableMap();
			newBitmap.deserialize(in);
			return newBitmap;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
