package accumulo.dao;

import java.util.List;

import org.roaringbitmap.longlong.Roaring64NavigableMap;

public interface Dao<T> {

	public long insert(T obj) throws Exception;

	public Roaring64NavigableMap queryIndex(String query) throws Exception;

	public byte[] readSingleDocument(long docId) throws Exception;

	public List<byte[]> readDocuments(Roaring64NavigableMap docIds, int nrMaxResults) throws Exception;

	public void delete(long docId) throws Exception;

}
