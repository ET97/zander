package accumulo.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.accumulo.core.client.BatchDeleter;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.MultiTableBatchWriter;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.Authorizations;
import org.json.JSONObject;
import org.roaringbitmap.longlong.LongIterator;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.query.AndIterator;
import accumulo.iterators.query.AndNotIterator;
import accumulo.iterators.query.OrIterator;
import accumulo.iterators.query.TermIterator;
import accumulo.iterators.query.XorIterator;
import accumulo.util.IdUtil;
import domain.ValueObject;

public class GenericDao implements Dao<ValueObject> {

	private static final byte[] NULL_BYTES = new byte[0];
	private static final byte[] DATA_BYTES = "DATA".getBytes();
	private static final Roaring64NavigableMapEncoder BITMAP_ENCODER = new Roaring64NavigableMapEncoder();
	private static final int NR_SHARDS = 10;
	private static final String DELETED_ROW = " deleted";
	private static final Range DELETED_RANGE = new Range(DELETED_ROW);
	private static final Range SEARCH_RANGE = new Range("-", null);
	private static final String DISCRIMINATE = ":";

	private Connector connector;
	private Authorizations auths;
	private String tableName;
	private String indexTableName;
	private long lastDocId;
	private int nrMaxIndices;

	public GenericDao(Connector connector, Authorizations auths, String tableName, String indexTableName,
			int nrMaxIndices) throws TableNotFoundException {
		this.connector = connector;
		this.auths = auths;
		this.tableName = tableName;
		this.indexTableName = indexTableName;
		lastDocId = readLastDocIdFromTable();
		this.nrMaxIndices = nrMaxIndices;
	}

	@Override
	public long insert(ValueObject obj) throws Exception {
		BatchWriterConfig config = new BatchWriterConfig();
		MultiTableBatchWriter multiWriter = connector.createMultiTableBatchWriter(config);
		try {
			lastDocId++;
			BatchWriter indexWriter = multiWriter.getBatchWriter(indexTableName);
			byte[] docValue = new Roaring64NavigableMapEncoder().encode(Roaring64NavigableMap.bitmapOf(lastDocId));
			indexWriter.addMutations(createIndexMutations(obj.getIndices(), docValue));
			BatchWriter docWriter = multiWriter.getBatchWriter(tableName);

			Mutation m = new Mutation(IdUtil.reverseId(Long.toString(lastDocId)).getBytes());
			m.put(DATA_BYTES, NULL_BYTES, obj.getContent());
			docWriter.addMutation(m);

			return lastDocId;
		} finally {
			multiWriter.close();
		}
	}

	@Override
	public Roaring64NavigableMap queryIndex(String query) throws Exception {
		Scanner indexScanner = connector.createScanner(indexTableName, auths);
		indexScanner.setRange(SEARCH_RANGE);
		indexScanner.addScanIterator(getIteratorSetting(30, query));
		Iterator<Entry<Key, Value>> iter = indexScanner.iterator();
		if (!iter.hasNext()) {
			return Roaring64NavigableMap.bitmapOf();
		}
		Roaring64NavigableMap firstEntry = BITMAP_ENCODER.decode(iter.next().getValue().get());
		while (iter.hasNext()) {
			firstEntry.or(BITMAP_ENCODER.decode(iter.next().getValue().get()));
		}
		indexScanner.clearColumns();
		indexScanner.setRange(DELETED_RANGE);
		Iterator<Entry<Key, Value>> delIter = indexScanner.iterator();
		if (delIter.hasNext()) {
			firstEntry.xor(BITMAP_ENCODER.decode(delIter.next().getValue().get()));
		}
		return firstEntry;
	}

	@Override
	public byte[] readSingleDocument(long docId) throws Exception {
		Scanner docScanner = connector.createScanner(tableName, auths);
		docScanner.setRange(new Range(IdUtil.reverseId(Long.toString(docId))));
		Iterator<Entry<Key, Value>> iter = docScanner.iterator();
		if (!iter.hasNext()) {
			return null;
		}
		return iter.next().getValue().get();
	}

	@Override
	public List<byte[]> readDocuments(Roaring64NavigableMap docIds, int nrMaxResults) throws Exception {
		List<byte[]> result = new ArrayList<>(nrMaxResults);
		Scanner docScanner = connector.createScanner(tableName, auths);
		LongIterator iter = docIds.getReverseLongIterator();
		int i = 0;
		while (iter.hasNext() && i < nrMaxResults) {
			String docId = IdUtil.reverseId(Long.toString(iter.next()));
			docScanner.setRange(new Range(docId));
			result.add(docScanner.iterator().next().getValue().get());
			i++;
		}
		docScanner.close();
		return result;
	}

	@Override
	public void delete(long docId) throws Exception {
		BatchWriter shardWriter = connector.createBatchWriter(indexTableName, new BatchWriterConfig());
		Mutation shardDel = new Mutation(DELETED_ROW);
		Roaring64NavigableMap delMap = Roaring64NavigableMap.bitmapOf(docId);
		shardDel.put(NULL_BYTES, NULL_BYTES, new Roaring64NavigableMapEncoder().encode(delMap));
		shardWriter.addMutation(shardDel);
		shardWriter.close();
		BatchDeleter docDeleter = connector.createBatchDeleter(tableName, auths, 5, new BatchWriterConfig());
		docDeleter.setRanges(Collections.singletonList(new Range(IdUtil.reverseId(Long.toString(docId)))));
		docDeleter.delete();
		docDeleter.close();
	}

	public int getMaxNrIndices() {
		return nrMaxIndices;
	}

	private long readLastDocIdFromTable() throws TableNotFoundException {
		Scanner scanner = connector.createScanner(indexTableName, auths);
		scanner.setRange(SEARCH_RANGE);
		if (!scanner.iterator().hasNext()) {
			return 0;
		}
		long max = 0L;
		Iterator<Entry<Key, Value>> iter = scanner.iterator();
		Roaring64NavigableMapEncoder encoder = new Roaring64NavigableMapEncoder();
		while (iter.hasNext()) {
			long thisMax = encoder.decode(iter.next().getValue().get()).getReverseLongIterator().next();
			if (max < thisMax) {
				max = thisMax;
			}
		}
		return max;
	}

	private List<Mutation> createIndexMutations(Map<String, String> indices, byte[] docValue) {
		String shard = Integer.toString((indices.hashCode() + docValue.hashCode()) % NR_SHARDS);
		List<Mutation> result = new ArrayList<>();
		indices.forEach((field, term) -> {
			Mutation m = new Mutation(shard);
			m.put((field + DISCRIMINATE + term).getBytes(), NULL_BYTES, docValue);
			result.add(m);
		});
		return result;
	}

	private IteratorSetting getIteratorSetting(int priority, String json) {
		JSONObject jsonObj = new JSONObject(json);
		switch (jsonObj.getString("type")) {
		case "TERM":
			return TermIterator.getIteratorSetting(priority, jsonObj.getString(TermIterator.FIELD),
					jsonObj.getString(TermIterator.VALUE));
		case "AND":
			return AndIterator.getIteratorSetting(priority, json);
		case "OR":
			return OrIterator.getIteratorSetting(priority, json);
		case "XOR":
			return XorIterator.getIteratorSetting(priority, json);
		case "ANDNOT":
			return AndNotIterator.getIteratorSetting(priority, json);
		default:
			throw new RuntimeException("Iterator type is unknown!");
		}
	}
}
