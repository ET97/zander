package domain;

import java.util.HashMap;
import java.util.Map;

public class ValueObject {

	/**
	 * indices containing entries of search field (key of map entry) and search term
	 * (value of map entry)
	 */
	private Map<String, String> indices;
	/**
	 * using byte array representation to be flexible in storing audio files, images
	 * etc.
	 */
	private byte[] content;

	public ValueObject() {
		indices = new HashMap<>(10);
	}

	public ValueObject(byte[] content) {
		this();
		this.content = content;
	}

	public ValueObject(Map<String, String> indices, byte[] content) {
		this.indices = indices;
		this.content = content;
	}

	public void addIndex(String key, String value) {
		indices.put(key, value);
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Map<String, String> getIndices() {
		return indices;
	}

	public byte[] getContent() {
		return content;
	}

}
