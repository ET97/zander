package util;

import domain.ValueObject;

public class Communication extends ValueObject {

	public Communication(int i) {
		super();
		addIndex("sender", "sender" + i);
		addIndex("receiver", "receiver" + i);
		addIndex("type", "email");
		addIndex("subject", "subject" + i);
		setContent(("content" + i).getBytes());
	}
}
