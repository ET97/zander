package accumulo.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.EnumSet;

import org.apache.accumulo.core.client.BatchDeleter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.iterators.IteratorUtil.IteratorScope;
import org.apache.accumulo.core.security.Authorizations;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.maintenance.LongBitmapCombiner;
import accumulo.util.MockAccumuloClient;
import domain.ValueObject;
import util.Communication;

public class GenericDaoTest {

	private static final Connector CONNECTOR = new MockAccumuloClient().getConnector();
	private static final Authorizations AUTHS = new Authorizations();
	private static final String TABLE_NAME = "test";
	private static final String INDEX_TABLE_NAME = "test_index";

	private static final String QUERY1 = "{\"type\": \"TERM\",\n\"field\": \"sender\",\n\"value\": \"sender0\"}";
	private static final String QUERY2 = "{\"type\": \"TERM\",\n\"field\": \"receiver\",\n\"value\": \"receiver0\"}";
	private static final String QUERY3 = "{\"type\": \"TERM\",\n\"field\": \"subject\",\n\"value\": \"subject0\"}";
	private static final String QUERY4 = "{\"type\": \"TERM\",\n\"field\": \"type\",\n\"value\": \"email\"}";

	private Dao<ValueObject> dao;

	@BeforeClass
	public static void setupAll() throws Exception {
		TableOperations to = CONNECTOR.tableOperations();
		to.create(INDEX_TABLE_NAME);
		to.create(TABLE_NAME);

		IteratorSetting cfg = new IteratorSetting(15, "indexCombiner", LongBitmapCombiner.class);
		LongBitmapCombiner.setCombineAllColumns(cfg, true);
		LongBitmapCombiner.setLossyness(cfg, true);

		to.attachIterator(INDEX_TABLE_NAME, cfg);
		to.removeIterator(INDEX_TABLE_NAME, "vers", EnumSet.allOf(IteratorScope.class));
	}

	@Before
	public void setup() throws Exception {
		BatchWriterConfig config = new BatchWriterConfig();
		Range range = new Range();

		BatchDeleter indDel = CONNECTOR.createBatchDeleter(INDEX_TABLE_NAME, AUTHS, 5, config);
		indDel.setRanges(Collections.singletonList(range));
		indDel.delete();
		indDel.close();

		BatchDeleter docDel = CONNECTOR.createBatchDeleter(TABLE_NAME, AUTHS, 5, config);
		docDel.setRanges(Collections.singletonList(range));
		docDel.delete();
		docDel.close();

		dao = new GenericDao(CONNECTOR, AUTHS, TABLE_NAME, INDEX_TABLE_NAME, 100);
	}

	@Test
	public void testInsert() throws Exception {
		dao.insert(new Communication(0));

		Scanner docScanner = CONNECTOR.createScanner(TABLE_NAME, AUTHS);
		assertTrue(docScanner.iterator().hasNext());

		Scanner indexScanner = CONNECTOR.createScanner(INDEX_TABLE_NAME, AUTHS);
		assertTrue(indexScanner.iterator().hasNext());
	}

	@Test
	public void testQueryIndex() throws Exception {
		dao.insert(new Communication(0));

		assertTrue(dao.queryIndex(QUERY1).contains(1L));
		assertTrue(dao.queryIndex(QUERY2).contains(1L));
		assertTrue(dao.queryIndex(QUERY3).contains(1L));
		assertTrue(dao.queryIndex(QUERY4).contains(1L));
	}

	@Test
	public void testComplexQuery_1() throws Exception {
		for (int i = 0; i < 2000; i++) {
			dao.insert(new Communication(i));
		}

		String query = "{\"type\": \"AND\",\"children\": [\n"
				+ "{\"type\": \"TERM\", \"field\": \"sender\",\"value\": \"sender1\"},\n"
				+ "{\"type\": \"TERM\", \"field\": \"type\",\"value\": \"email\"},\n"
				+ "{\"type\": \"TERM\", \"field\": \"receiver\",\"value\": \"receiver1\"}]}\n";
		Roaring64NavigableMap bitmap = dao.queryIndex(query);
		assertEquals(Roaring64NavigableMap.bitmapOf(2L), bitmap);
	}

	@Test
	public void testComplexQuery_2() throws Exception {
		for (int i = 0; i < 2000; i++) {
			dao.insert(new Communication(i));
		}

		String query = "{\n\"type\": \"OR\",\"children\": [\n" + "{\"type\": \"AND\",\"children\": [\n"
				+ "{\"type\": \"TERM\", \"field\": \"type\",\"value\": \"email\"},\n"
				+ "{\"type\": \"TERM\", \"field\": \"sender\",\"value\": \"sender1\"},\n"
				+ "{\"type\": \"TERM\", \"field\": \"receiver\",\"value\": \"receiver1\"}]},\n"
				+ "{\"type\": \"XOR\",\"children\": [\n"
				+ "{\"type\": \"TERM\", \"field\": \"subject\",\"value\": \"subject3\"},\n"
				+ "{\"type\": \"TERM\", \"field\": \"sender\",\"value\": \"sender200\"}]},\n"
				+ "{\"type\": \"TERM\", \"field\": \"sender\",\"value\": \"sender340\"},"
				+ "{\"type\": \"ANDNOT\", \"posChild\": "
				+ "{\"type\": \"TERM\", \"field\": \"subject\",\"value\": \"subject5\"},\n" + "\"negChild\": "
				+ "{\"type\": \"TERM\", \"field\": \"subject\",\"value\": \"subject3\"}}]}";

		Roaring64NavigableMap bitmap = dao.queryIndex(query);
		assertEquals(Roaring64NavigableMap.bitmapOf(2L, 4L, 6L, 201L, 341L), bitmap);
	}

	@Test
	public void testReadDocument() throws Exception {
		dao.insert(new Communication(0));

		assertEquals(1, dao.readDocuments(Roaring64NavigableMap.bitmapOf(1L), 100).size());
		assertEquals("content0", new String(dao.readDocuments(Roaring64NavigableMap.bitmapOf(1L), 100).get(0)));
	}

	@Test
	public void testDelete() throws Exception {
		Roaring64NavigableMapEncoder encoder = new Roaring64NavigableMapEncoder();
		long id = dao.insert(new Communication(0));

		dao.delete(id);

		Scanner docScanner = CONNECTOR.createScanner(TABLE_NAME, AUTHS);
		assertFalse(docScanner.iterator().hasNext());

		Scanner indexScanner = CONNECTOR.createScanner(INDEX_TABLE_NAME, AUTHS);
		indexScanner.setRange(new Range("-", null));
		assertEquals(Roaring64NavigableMap.bitmapOf(id),
				encoder.decode(indexScanner.iterator().next().getValue().get()));

		indexScanner.setRange(new Range(" deleted"));
		assertEquals(Roaring64NavigableMap.bitmapOf(id),
				encoder.decode(indexScanner.iterator().next().getValue().get()));
	}
}
