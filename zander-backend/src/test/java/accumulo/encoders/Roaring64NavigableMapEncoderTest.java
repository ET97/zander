package accumulo.encoders;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

public class Roaring64NavigableMapEncoderTest {

	@Test
	public void test() {
		Roaring64NavigableMapEncoder encoder = new Roaring64NavigableMapEncoder();
		Roaring64NavigableMap bitmap1 = Roaring64NavigableMap.bitmapOf(1L, 45L, 3423L, 6556L, 456L);
		byte[] dummy = encoder.encode(bitmap1);

		Roaring64NavigableMap bitmap2 = encoder.decode(dummy);
		assertEquals(bitmap1, bitmap2);
	}

}
