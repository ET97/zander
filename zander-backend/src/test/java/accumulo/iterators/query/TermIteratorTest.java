package accumulo.iterators.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.hadoop.io.Text;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import accumulo.iterators.util.DummyIterator;
import accumulo.util.AccumuloClient;
import accumulo.util.MockAccumuloClient;

public class TermIteratorTest {

	private static AccumuloClient client;
	private static Connector connector;
	private static String table = "test";
	private static String field = "field1";
	private static String value = "value1";
	private static String content = "content1";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		client = new MockAccumuloClient();
		connector = client.getConnector();

		connector.tableOperations().create(table);
		insertTestData();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		client.close();
	}

	@Test
	public void test() throws Exception {
		Scanner scanner = connector.createScanner(table, new Authorizations());
		scanner.addScanIterator(TermIterator.getIteratorSetting(30, field, value));
		Iterator<Entry<Key, Value>> iter = scanner.iterator();
		assertTrue(iter.hasNext());
		Entry<Key, Value> entry = iter.next();
		assertEquals(new Text("1"), entry.getKey().getRow());
		assertEquals(new Text(field + ":" + value), entry.getKey().getColumnFamily());
		assertEquals(content, new String(entry.getValue().get()));

		assertTrue(iter.hasNext());
		entry = iter.next();
		assertEquals(new Text("3"), entry.getKey().getRow());
		assertEquals(new Text(field + ":" + value), entry.getKey().getColumnFamily());
		assertEquals(content, new String(entry.getValue().get()));

		assertFalse(iter.hasNext());
	}

	@Test
	public void testCreationFromJson() throws Exception {
		SortedKeyValueIterator<Key, Value> dummyIter = new DummyIterator();
		TermIterator iter1 = new TermIterator(dummyIter, field, value);
		TermIterator iter2 = TermIterator.createFromJson(dummyIter, iter1.describeAsJson());
		assertEquals(iter1.describeAsJson(), iter2.describeAsJson());
	}

	private static void insertTestData() throws TableNotFoundException, MutationsRejectedException {
		BatchWriter writer = connector.createBatchWriter(table, new BatchWriterConfig());

		Mutation m1 = new Mutation("1");
		m1.put(field + ":" + value, "", content);
		writer.addMutation(m1);

		Mutation m2 = new Mutation("1");
		m2.put(field + 2 + ":" + value + 2, "", content + 2);
		writer.addMutation(m2);

		Mutation m3 = new Mutation("3");
		m3.put(field + ":" + value, "", content);
		writer.addMutation(m3);

		writer.close();
	}
}
