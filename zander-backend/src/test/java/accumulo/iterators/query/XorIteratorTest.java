package accumulo.iterators.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.security.Authorizations;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.util.DummyIterator;
import accumulo.iterators.util.DummyIteratorEnvironment;
import accumulo.util.AccumuloClient;
import accumulo.util.MockAccumuloClient;

public class XorIteratorTest {

	private static final byte[] NULL_BYTES = "".getBytes();
	private static final Roaring64NavigableMapEncoder ENCODER = new Roaring64NavigableMapEncoder();
	private static final String child1Json = "{\n\"type\": \"TERM\",\n\"field\": \"field1\",\n\"value\": \"value1\"\n}";
	private static final String child2Json = "{\n\"type\": \"TERM\",\n\"field\": \"field2\",\n\"value\": \"value2\"\n}";
	private static final String child3Json = "{\n\"type\": \"TERM\",\n\"field\": \"field\",\n\"value\": \"value\"\n}";
	private static final SortedKeyValueIterator<Key, Value> dummyIter = new DummyIterator();
	private static final IteratorEnvironment dummyEnv = new DummyIteratorEnvironment();

	private static AccumuloClient client;
	private static Connector connector;
	private static String table = "test";
	private static String field = "field";
	private static String value = "value";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		client = new MockAccumuloClient();
		connector = client.getConnector();

		connector.tableOperations().create(table);
		insertTestData();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		client.close();
	}

	@Test
	public void testCreatFromJson() throws Exception {
		XorIterator iter1 = new XorIterator(Arrays.asList(TermIterator.createFromJson(dummyIter, child1Json),
				TermIterator.createFromJson(dummyIter, child2Json)));
		XorIterator iter2 = XorIterator.createFromJson(dummyIter, dummyEnv, iter1.describeAsJson());
		assertEquals(iter1.describeAsJson(), iter2.describeAsJson());
	}

	@Test
	public void test_1() throws Exception {
		XorIterator iter1 = new XorIterator(Arrays.asList(TermIterator.createFromJson(dummyIter, child1Json),
				TermIterator.createFromJson(dummyIter, child2Json)));
		String json = iter1.describeAsJson();

		Scanner scanner = connector.createScanner(table, new Authorizations());
		scanner.addScanIterator(XorIterator.getIteratorSetting(30, json));
		Iterator<Entry<Key, Value>> iter = scanner.iterator();
		assertTrue(iter.hasNext());
		Entry<Key, Value> entry = iter.next();
		assertEquals(Roaring64NavigableMap.bitmapOf(5L, 12L, 82L), ENCODER.decode(entry.getValue().get()));

		iter.forEachRemaining(System.out::println);
		assertFalse(iter.hasNext());
	}

	@Test
	public void test_2() throws Exception {
		List<QueryIterator> childs = new ArrayList<>(2);
		childs.add(TermIterator.createFromJson(dummyIter, child1Json));
		childs.add(TermIterator.createFromJson(dummyIter, child3Json));
		XorIterator iter1 = new XorIterator(childs);
		String json = iter1.describeAsJson();

		Scanner scanner = connector.createScanner(table, new Authorizations());
		scanner.addScanIterator(XorIterator.getIteratorSetting(30, json));
		Iterator<Entry<Key, Value>> iter = scanner.iterator();
		assertTrue(iter.hasNext());
		Entry<Key, Value> entry = iter.next();
		assertEquals(Roaring64NavigableMap.bitmapOf(1L, 5L, 12L), ENCODER.decode(entry.getValue().get()));

		assertTrue(iter.hasNext());
		entry = iter.next();
		assertEquals(Roaring64NavigableMap.bitmapOf(2L, 13L, 15L), ENCODER.decode(entry.getValue().get()));

		assertFalse(iter.hasNext());
	}

	private static void insertTestData() throws TableNotFoundException, MutationsRejectedException {
		BatchWriter writer = connector.createBatchWriter(table, new BatchWriterConfig());

		Mutation m1 = new Mutation("1");
		m1.put((field + 1 + ":" + value + 1).getBytes(), NULL_BYTES,
				ENCODER.encode(Roaring64NavigableMap.bitmapOf(1L, 12L, 5L)));
		writer.addMutation(m1);

		Mutation m2 = new Mutation("1");
		m2.put((field + 2 + ":" + value + 2).getBytes(), NULL_BYTES,
				ENCODER.encode(Roaring64NavigableMap.bitmapOf(1L, 82L)));
		writer.addMutation(m2);

		Mutation m3 = new Mutation("3");
		m3.put((field + ":" + value).getBytes(), NULL_BYTES,
				ENCODER.encode(Roaring64NavigableMap.bitmapOf(13L, 2L, 15L)));
		writer.addMutation(m3);

		writer.close();
	}
}
