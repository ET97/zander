package accumulo.iterators.util;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;

public class DummyIterator implements SortedKeyValueIterator<Key, Value> {

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean hasTop() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void next() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies, boolean inclusive) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public Key getTopKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value getTopValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		return this;
	}

}
