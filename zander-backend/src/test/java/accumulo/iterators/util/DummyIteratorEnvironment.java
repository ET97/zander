package accumulo.iterators.util;

import java.io.IOException;

import org.apache.accumulo.core.client.sample.SamplerConfiguration;
import org.apache.accumulo.core.conf.AccumuloConfiguration;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.IteratorUtil.IteratorScope;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.security.Authorizations;

public class DummyIteratorEnvironment implements IteratorEnvironment {

	@Override
	public SortedKeyValueIterator<Key, Value> reserveMapFileReader(String mapFileName) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccumuloConfiguration getConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IteratorScope getIteratorScope() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isFullMajorCompaction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerSideChannel(SortedKeyValueIterator<Key, Value> iter) {
		// TODO Auto-generated method stub

	}

	@Override
	public Authorizations getAuthorizations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IteratorEnvironment cloneWithSamplingEnabled() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSamplingEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SamplerConfiguration getSamplerConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
