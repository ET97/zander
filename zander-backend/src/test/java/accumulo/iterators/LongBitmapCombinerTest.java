package accumulo.iterators;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;

import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorUtil.IteratorScope;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.hadoop.io.Text;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.maintenance.LongBitmapCombiner;
import accumulo.util.AccumuloClient;
import accumulo.util.MockAccumuloClient;

public class LongBitmapCombinerTest {

	private static final String TABLE_NAME = "test";
	private static final Text ROW = new Text("row");
	private static final byte[] CF = "cf".getBytes();
	private static final byte[] CQ = "cq".getBytes();
	private static final Roaring64NavigableMapEncoder ENCODER = new Roaring64NavigableMapEncoder();

	private static AccumuloClient CLIENT;
	private static Connector CONNECTOR;

	@BeforeClass
	public static void setup() throws Exception {
		CLIENT = new MockAccumuloClient();
		CONNECTOR = CLIENT.getConnector();
		CONNECTOR.tableOperations().create(TABLE_NAME);
		CONNECTOR.tableOperations().removeIterator(TABLE_NAME, "vers", EnumSet.allOf(IteratorScope.class));
	}

	@AfterClass
	public static void shutdown() throws Exception {
		CLIENT.close();
	}

	@Before
	public void prepare() throws Exception {
		CONNECTOR.tableOperations().deleteRows(TABLE_NAME, null, null);
		try (BatchWriter writer = CONNECTOR.createBatchWriter(TABLE_NAME, new BatchWriterConfig())) {

			Mutation mut1 = new Mutation(ROW);
			byte[] value1 = ENCODER.encode(Roaring64NavigableMap.bitmapOf(1L, 34L));
			mut1.put(CF, CQ, 1L, value1);
			writer.addMutation(mut1);
			writer.flush();

			Mutation mut2 = new Mutation(ROW);
			byte[] value2 = ENCODER.encode(Roaring64NavigableMap.bitmapOf(12L));
			mut2.put(CF, CQ, 2L, value2);
			writer.addMutation(mut2);
			writer.close();
		}
	}

	@Test
	public void test() throws Exception {
		Scanner scanner = CONNECTOR.createScanner(TABLE_NAME, new Authorizations());
		IteratorSetting setting = new IteratorSetting(30, "bmCombiner", LongBitmapCombiner.class);
		LongBitmapCombiner.setCombineAllColumns(setting, true);
		LongBitmapCombiner.setLossyness(setting, true);
		scanner.addScanIterator(setting);
		scanner.setRange(new Range());
		List<Entry<Key, Value>> tableEntries = new ArrayList<>();
		scanner.forEach(tableEntries::add);
		assertEquals(1, tableEntries.size());
		Roaring64NavigableMap values = Roaring64NavigableMap.bitmapOf(1L, 12L, 34L);
		assertEquals(values, ENCODER.decode(tableEntries.get(0).getValue().get()));
	}
}
