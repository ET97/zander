package app;

import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.admin.CompactionConfig;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorUtil.IteratorScope;
import org.apache.accumulo.core.iterators.TypedValueCombiner.Encoder;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.hadoop.io.Text;
import org.roaringbitmap.longlong.Roaring64NavigableMap;

import accumulo.dao.Dao;
import accumulo.dao.GenericDao;
import accumulo.encoders.Roaring64NavigableMapEncoder;
import accumulo.iterators.maintenance.LongBitmapCombiner;
import accumulo.util.AccumuloClient;
import accumulo.util.IdUtil;
import accumulo.util.MiniAccumuloClient;
import domain.ValueObject;

public class ShowTables {

	private static final String INDEX_TABLE = "communication_index";
	private static final String DOC_TABLE = "communication";
	private static final SortedSet<Text> DOC_TABLE_SPLITS = IdUtil.createSplits(100);

	private static final String QUERY = "{\"type\": \"TERM\",\n\"searchField\": \"type\",\n\"searchValue\": \"email\"}";

	public static void main(String[] args) throws Exception {
		try (AccumuloClient client = new MiniAccumuloClient("pass")) {
			Connector connector = client.getConnector();

			Encoder<Roaring64NavigableMap> encoder = new Roaring64NavigableMapEncoder();

			TableOperations to = connector.tableOperations();

			connector.tableOperations().create(DOC_TABLE);
			connector.tableOperations().addSplits(DOC_TABLE, DOC_TABLE_SPLITS);
			connector.tableOperations().create(INDEX_TABLE);
			Authorizations auths = new Authorizations();
			Dao<ValueObject> dao = new GenericDao(connector, auths, DOC_TABLE, INDEX_TABLE, 100);

			IteratorSetting cfg = new IteratorSetting(15, "indexCombiner", LongBitmapCombiner.class);
			LongBitmapCombiner.setCombineAllColumns(cfg, true);
			LongBitmapCombiner.setLossyness(cfg, true);

			LongBitmapCombiner.testEncoder(encoder, Roaring64NavigableMap.bitmapOf(1L));
			to.attachIterator(INDEX_TABLE, cfg);
			to.removeIterator(INDEX_TABLE, "vers", EnumSet.allOf(IteratorScope.class));

			for (int i = 0; i < 2000; i++) {
				dao.insert(new Communication(i));
			}

			dao.delete(1L);

			Scanner indexScanner = connector.createScanner(INDEX_TABLE, auths);
			for (Entry<Key, Value> entry : indexScanner) {
				System.out.println(entry.getKey());
				byte[] value = entry.getValue().get();
				Roaring64NavigableMap bitmap = encoder.decode(value);
				System.out.println(bitmap);
			}

			to.compact(INDEX_TABLE, new CompactionConfig());

			Scanner docScanner = connector.createScanner(DOC_TABLE, auths);
			for (Entry<Key, Value> entry : docScanner) {
				System.out.println(entry.getKey());
				System.out.println(new String(entry.getValue().get()));
			}

			dao.delete(1L);

			Roaring64NavigableMap list = dao.queryIndex(QUERY);
			System.out.println(list.isEmpty());
			System.out.println(list.getIntCardinality());
			List<byte[]> res = dao.readDocuments(list, 100);
			System.out.println(res.size());
			System.out.println(new String(res.get(0)));
			System.out.println(new String(res.get(5)));
			System.out.println(new String(res.get(res.size() - 1)));
		}
	}

	private static class Communication extends ValueObject {
		private Communication(int i) {
			super();
			addIndex("sender", "sender" + i);
			addIndex("receiver", "receiver" + i);
			addIndex("type", "email");
			setContent(("content" + i).getBytes());
		}

		@Override
		public String toString() {
			return getIndices().toString() + " : " + new String(getContent());
		}
	}

}
